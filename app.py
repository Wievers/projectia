# Packages import
import pandas
import numpy as np
import logging, os
from termcolor import cprint
from matplotlib import pyplot as plt
import argparse as ap

# Local imports
import featureAnalysis as fa
import unsupTraining as ut
np.random.seed(0)

# On récupère le chemin du dossier courant
BASEDIR = os.path.dirname(os.path.abspath(__file__))
os.chdir(BASEDIR)

# Logging configuration
logger = logging.getLogger(__name__)
logging.getLogger('matplotlib.font_manager').disabled = True

parser = ap.ArgumentParser(prog="PyratPredictor", description='A program computing multiple ML solutions for a pyrat game')
parser.add_argument('-ma', '--method-analyze', help='The method to use for features analysis', required=True, choices=["pca", "tsne", "none"])
parser.add_argument('--min-nb-cluster', help= 'The minimum number of clusters to test', default=2, type=int)
parser.add_argument('--max-nb-cluster', help= 'The maximum number of clusters to test', default=100, type=int)
parser.add_argument('-va', '--variance', help= 'The variance to keep if we use the PCA analysis', default=0.9, type=float)
parser.add_argument('-mc', '--method-clustering', help='The method to use for clustering (kmeans, spectral or agglomerative)', required=True, choices=["kmeans", "agglomerative", "spectral"])
parser.add_argument('--demonstrator', help='The demonstrator to use for the supervised training', type=bool, default=False)
args = parser.parse_args()

# Environment variables
os.environ["DEMO"] = "1" if args.demonstrator else "0"

class PyratPredictor:
    def __init__(self, methodAnalyzes="pca", methodClustering="kmeans"):
        self.raw_dataset = pandas.read_csv("datasets/features_unsupervised.csv", sep=";")
        self.pcaFA = None
        self.predictions = None
        self._methodAnalyse = methodAnalyzes
        self._methodClustering = methodClustering
    
    def organize_features(self, variance, reduce_dim=True):
        self.pcaFA = fa.FeatureAnalysis(self.raw_dataset, variance, method=self._methodAnalyse)
        self.predictions = ut.UnsupervisedTraining(self.pcaFA.pruned_dataset if reduce_dim else self.pruned_dataset, self._methodAnalyse)
        
    def find_cluster(self):
        if self.pcaFA is None:
            logger.error("You must organize features first!")
            return None
        
        if self._methodClustering == "kmeans":
            pred = self.predictions.kmeanClustering(min_nbcluster=args.min_nb_cluster, max_nbcluster=args.max_nb_cluster)
        elif self._methodClustering == "agglomerative":
            pred = self.predictions.agglomerativeClustering(min_nbcluster=args.min_nb_cluster, max_nbcluster=args.max_nb_cluster)
        elif self._methodClustering == "spectral":
            pred = self.predictions.spectralClustering(min_nbcluster=args.min_nb_cluster, max_nbcluster=args.max_nb_cluster)

        return pred
        
    def create_image(self):
        # Get the 10 first lines of the dataset
        idx_closest_centroids = self.find_cluster()
        rdIndices = np.random.choice(self.pcaFA.pruned_dataset.shape[0], 4, replace=False)

        closest_centroids = self.predictions.kmeans["model"].cluster_centers_[idx_closest_centroids]

        # On récupère les indices aléatoires:
        allCalc = zip([self.pcaFA.pruned_dataset[i] for i in rdIndices],[closest_centroids[i] for i in rdIndices])

        # Get the closest centroid for each line
        for i,(curcen, im_cen) in enumerate(allCalc):
            plt.subplot(2, 4, 2*i+1)
            plt.imshow(curcen.reshape(5,6), cmap=plt.cm.Blues)
            plt.xticks([])
            plt.yticks([])
            plt.title("Original")

            plt.subplot(2, 4, 2+2*i)
            plt.imshow(im_cen.reshape(5,6), cmap=plt.cm.Blues)
            plt.xticks([])
            plt.yticks([])
            plt.title("Closest centroid, distance")
            plt.suptitle("Original vs Closest centroid, distance")

        # Create the image
        plt.savefig(f"graphs/centroidtoimage{self.predictions.kmeans['model'].n_clusters}.png", dpi = 150)
        plt.close()

if __name__ == "__main__":
    cprint("Prediction for PyRat", "red")
    cprint("---------------------", "red")
    predictor = PyratPredictor(methodAnalyzes=args.method_analyze, methodClustering=args.method_clustering)
    predictor.organize_features(args.variance)
    predictor.find_cluster()
    #predictor.create_image()
