from sklearn.cluster import KMeans, AgglomerativeClustering, SpectralClustering
from sklearn.metrics import silhouette_score, calinski_harabasz_score, davies_bouldin_score
from termcolor import cprint
import tqdm
import numpy as np

from utils import _grapher, _grapherColor, METHODS

class UnsupervisedTraining:
    def __init__(self, dataset, method="pca"):
        self.dataset = dataset
        self._method = method
        self.kmeans = dict()

    def _generator(self, allParamSil, allParamCal, allParamDav, min_nbcluster, max_nbcluster, calClust, davClust, nbClusters, prediction, method : str):
        if self._method == METHODS[1]:
            _grapher(allParamSil, f"Silhouette Parameter for T-SNE analysed data with {method}", 
                     "Number of clusters", "Silhouette Parameter", 
                     f'graphs/silhouette_{method.lower()}_tsne{nbClusters}.png', 
                     legendPlot=f'Best Number of clusters: {nbClusters}, max silhouette: {round(max(allParamSil), 3)}',
                     x = range(min_nbcluster, max_nbcluster))
            _grapher(allParamCal, f"Calinski-Harabasz Parameter for T-SNE analysed data with {method}", 
                     "Number of clusters", "Calinski-Harabasz Parameter", 
                     f'graphs/calinski_{method.lower()}_tsne{nbClusters}.png', 
                     legendPlot=f'Best Number of clusters: {calClust}, max Calinski-Harabasz: {round(max(allParamCal), 3)}',
                     x = range(min_nbcluster, max_nbcluster))
            _grapher(allParamDav, f"Davies-Bouldin Parameter for T-SNE analysed data with {method}", 
                     "Number of clusters", "Davies-Bouldin Parameter", 
                     f'graphs/davies_{method.lower()}_tsne{nbClusters}.png', 
                     legendPlot=f'Best Number of clusters: {davClust}, min Davies-Bouldin: {round(max(allParamDav), 3)}',
                     x = range(min_nbcluster, max_nbcluster))
            _grapherColor([self.dataset[:,0], self.dataset[:,1], prediction], f"Clustering using {method} (TSNE Analyzed data)", 
                          "Feature 1", "Feature 2", f'graphs/clustering_{method.lower()}_tsne{nbClusters}.png')
            
            np.savez_compressed(f'labels/labels_{method.lower().replace(" ", "_")}_tsne_{nbClusters}.npz',labels=prediction)

        elif self._method == METHODS[0]:
            _grapher(allParamSil, f"Silhouette Parameter for PCA analysed datas with {method}", "Number of clusters", 
                     "Silhouette Parameter", f'graphs/silhouette_{method.lower()}_pca{nbClusters}.png', 
                     legendPlot=f'Best Number of clusters: {nbClusters}, max silhouette: {round(max(allParamSil), 3)}',
                     x = range(min_nbcluster, max_nbcluster))
            _grapher(allParamCal, f"Calinski-Harabasz Parameter for PCA analysed datas with {method}", 
                     "Number of clusters", "Calinski-Harabasz Parameter", f'graphs/calinski_{method.lower()}_pca{nbClusters}.png', 
                     legendPlot=f'Best Number of clusters: {calClust}, max Calinski-Harabasz: {round(max(allParamCal), 3)}',
                     x = range(min_nbcluster, max_nbcluster))
            _grapher(allParamDav, f"Davies-Bouldin Parameter for PCA analysed datas with {method}", 
                     "Number of clusters", "Davies-Bouldin Parameter", f'graphs/davies_{method.lower()}_pca{nbClusters}.png', 
                     legendPlot=f'Best Number of clusters: {davClust}, min Davies-Bouldin: {round(max(allParamDav), 3)}',
                     x = range(min_nbcluster, max_nbcluster))

            np.savez_compressed(f'labels/labels_{method.lower().replace(" ", "_")}_pca_{nbClusters}.npz',labels=prediction)

    def kmeanClustering(self, min_nbcluster = 2, max_nbcluster = 10):
        """
        Performs KMeans clustering on the dataset.
        KMeans algorithm is an Exclusive and Overlapping Clustering algorithm. 
        It is an iterative algorithm that aims to partition the dataset into K clusters in which each observation belongs to the cluster with the nearest mean.
        Here, we get the best number of clusters by using the Silhouette coefficients.
        Parameters:
        ----------
            min_nbcluster (int): The minimum number of clusters to test.
            max_nbcluster (int): The maximum number of clusters to test.
        Returns:
        -------
            The predictions of the KMeans model. """
        # Paramètres de silhouette
        allParSil = list()
        allParCal = list()
        allParDav = list()
        paramSil = -1
        paramCal = 0
        calClust = 0
        paramDav = np.inf
        davClust = 0

        # Vérification des paramètres
        max_nbcluster = max_nbcluster if max_nbcluster >= min_nbcluster else min_nbcluster + 1

        cprint("---- Evaluation du coefficient de Silhouette ----", "green", attrs=["bold"])
        for i in tqdm.tqdm(range(min_nbcluster, max_nbcluster), desc=f"KMeans"):
            silhouette_avg = paramSil
            clHarb = paramCal

            # KMeans définition
            kmeans=KMeans(n_clusters=i, init='k-means++',random_state=1, n_init="auto")
            kmeans.fit(self.dataset)
            prediction = kmeans.predict(self.dataset)

            # Quality analysis Parameters
            silhouette_avg = silhouette_score(self.dataset, kmeans.labels_)
            clHarb = calinski_harabasz_score(self.dataset, kmeans.labels_)
            davBoul = davies_bouldin_score(self.dataset, kmeans.labels_)

            allParSil.append(silhouette_avg)
            allParCal.append(clHarb)
            allParDav.append(davBoul)

            # On garde le meilleur coefficient de Davies-Bouldin
            if davBoul < paramDav:
                davClust = i
                paramDav = davBoul

            # On garde le meilleur coefficient de Calinski-Harabasz
            if clHarb > paramCal:
                calClust = i
                paramCal = clHarb

            # On garde le meilleur coefficient de silhouette
            if silhouette_avg > paramSil:
                self.kmeans = {"model" : kmeans, "silhouette" : silhouette_avg, "calinski" : clHarb, "prediction" : prediction}
                paramSil = silhouette_avg

        print(f"Best silhouette: {self.kmeans['silhouette']}")

        # Affichage du coefficient de silhouette sur un graph
        self._generator(allParSil, allParCal, allParDav, min_nbcluster, max_nbcluster, calClust, davClust, self.kmeans["model"].n_clusters, self.kmeans["prediction"], "KMeans")

        return self.kmeans["prediction"]
    
    def agglomerativeClustering(self, min_nbcluster = 2, max_nbcluster = 100):
        # Paramètres de silhouette
        allParSil = list()
        allParCal = list()
        allParDav = list()
        paramSil = -1
        paramCal = 0
        calClust = 0
        paramDav = np.inf
        davClust = 0

        # Vérification des paramètres
        max_nbcluster = max_nbcluster if max_nbcluster >= min_nbcluster else min_nbcluster + 1

        cprint("---- Evaluation du coefficient de Silhouette ----", "green", attrs=["bold"])
        for i in tqdm.tqdm(range(min_nbcluster, max_nbcluster), desc=f"Agglomerative Clustering"):
            silhouette_avg = paramSil
            clHarb = paramCal

            # Agglomerative Clustering définition
            aggloClust=AgglomerativeClustering(n_clusters=i, metric='euclidean', linkage='ward')
            prediction = aggloClust.fit_predict(self.dataset)

            # Quality analysis Parameters
            silhouette_avg = silhouette_score(self.dataset, aggloClust.labels_)
            clHarb = calinski_harabasz_score(self.dataset, aggloClust.labels_)
            davBoul = davies_bouldin_score(self.dataset, aggloClust.labels_)

            # On ajoute les paramètres
            allParSil.append(silhouette_avg)
            allParCal.append(clHarb)
            allParDav.append(davBoul)

            # On garde le meilleur coefficient de Davies-Bouldin
            if davBoul < paramDav:
                davClust = i
                paramDav = davBoul

            # On garde le meilleur coefficient de Calinski-Harabasz
            if clHarb > paramCal:
                calClust = i
                paramCal = clHarb

            # On garde le meilleur coefficient de silhouette
            if silhouette_avg > paramSil:
                self.agglomerative = {"model" : aggloClust, "silhouette" : silhouette_avg, "calinski" : clHarb, "prediction" : prediction}
                paramSil = silhouette_avg

        print(f"Best silhouette: {self.agglomerative['silhouette']}")
        print(self.agglomerative["prediction"])
        # Affichage du coefficient de silhouette sur un graph
        self._generator(allParSil, allParCal, allParDav, min_nbcluster, max_nbcluster, calClust, davClust, self.agglomerative["model"].n_clusters, self.agglomerative["prediction"], "Agglomerative Clustering")

        return self.agglomerative["prediction"]

    def spectralClustering(self, min_nbcluster = 2, max_nbcluster = 100):
        # Paramètres de silhouette
        allParSil = list()
        allParCal = list()
        allParDav = list()
        paramSil = -1
        paramCal = 0
        calClust = 0
        paramDav = np.inf
        davClust = 0

        # Vérification des paramètres
        max_nbcluster = max_nbcluster if max_nbcluster >= min_nbcluster else min_nbcluster + 1

        cprint("---- Evaluation du coefficient de Silhouette ----", "green", attrs=["bold"])
        for i in tqdm.tqdm(range(min_nbcluster, max_nbcluster), desc=f"Spectral Clustering"):
            silhouette_avg = paramSil
            clHarb = paramCal

            # Spectral Clustering définition
            spectralClust = SpectralClustering(n_clusters=i, eigen_solver='arpack',assign_labels='kmeans', n_jobs=3)
            prediction = spectralClust.fit_predict(self.dataset)

            # Quality analysis Parameters
            silhouette_avg = silhouette_score(self.dataset, spectralClust.labels_)
            clHarb = calinski_harabasz_score(self.dataset, spectralClust.labels_)
            davBoul = davies_bouldin_score(self.dataset, spectralClust.labels_)

            # On ajoute les paramètres
            allParSil.append(silhouette_avg)
            allParCal.append(clHarb)
            allParDav.append(davBoul)

            # On garde le meilleur coefficient de Davies-Bouldin
            if davBoul < paramDav:
                davClust = i
                paramDav = davBoul

            # On garde le meilleur coefficient de Calinski-Harabasz
            if clHarb > paramCal:
                calClust = i
                paramCal = clHarb

            # On garde le meilleur coefficient de silhouette
            if silhouette_avg > paramSil:
                self.spectral = {"model" : spectralClust, "silhouette" : silhouette_avg, "calinski" : clHarb, "prediction" : prediction}
                paramSil = silhouette_avg
            

        print(f"Best silhouette: {self.spectral['silhouette']}")
        
        # Affichage du coefficient de silhouette sur un graph
        self._generator(allParSil, allParCal, allParDav, min_nbcluster, max_nbcluster, calClust, davClust, self.spectral["model"].n_clusters, self.spectral["prediction"], "Spectral Clustering")

        return self.spectral["prediction"]