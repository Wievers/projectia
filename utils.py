import matplotlib.pyplot as plt
import os

METHODS = ["pca", "tsne", "none"]

def _grapher(plot, title, xtitle, ytitle, figTitle, legendPlot=None, x=None):
    x = x if x is not None else range(len(plot))
    fig, axs = plt.subplots()
    axs.set_title(title)
    axs.set_xlabel(xtitle)
    axs.set_ylabel(ytitle)
    axs.grid()
    axs.plot(x, plot, linestyle='--', marker="o", label=legendPlot)
    axs.legend()
    if os.getenv("DEMO", "0") == "1":
        plt.show()
    else:
        plt.savefig(figTitle, dpi = 150)
    plt.close()

def _grapherColor(plot, title, xtitle, ytitle, figTitle):
    fig, axs = plt.subplots()
    axs.set_title(title)
    axs.set_xlabel(xtitle)
    axs.set_ylabel(ytitle)
    axs.grid()
    axs.scatter(plot[0], plot[1], c=plot[2])
    if os.getenv("DEMO", "0") == "1":
        plt.show()
    else:
        plt.savefig(figTitle, dpi = 150)
    plt.close()