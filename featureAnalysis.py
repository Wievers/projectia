from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.manifold import TSNE
import pandas as pd
import numpy as np
from numpy.linalg import norm
import logging, os
from termcolor import cprint
from matplotlib import pyplot as plt
logging.getLogger('matplotlib.font_manager').disabled = True

import utils as uls

class FeatureAnalysis:
    """
        A class that performs feature analysis on a given dataset using PCA.

        Attributes:
        -----------
        dataset : pandas.DataFrame
            The dataset to perform feature analysis on.
        pca : sklearn.decomposition.PCA
            The PCA object used to perform the analysis.
        pruned_dataset : pandas.DataFrame
            Dataset with reduced dimensions and with scaled features.

        Methods:
        --------
        pcaAnalysis(variance: float) -> None:
            Performs PCA on the dataset and keeps the number of components that explain
            at least the given variance."""
    def __init__(self, dataset, variance, method="pca"):
        self.dataset = dataset
        if method == "tsne":
            self.tsneAnalysis(variance)
        elif method == "pca":
            self.pcaAnalysis(variance)
        else:
            self.pruned_dataset = self.dataset

    def _correlation(self):
        """
        Calculates the correlation matrix of the dataset.

        Returns:
        --------
        - The correlation matrix.
        """
        print(self.dataset.corr())
        print(min(self.dataset.corr()))
        return self.dataset.corr()
    
    def _scaler(self):
        """
        Scale the dataset before applying PCA.

        Returns:
        --------
        - The scaled dataset.
        """
        scaling=StandardScaler()
        scaling.fit(self.dataset)
        scaled_data = scaling.transform(self.dataset)
        dataset_scaled = pd.DataFrame(scaled_data, columns=scaling.get_feature_names_out())
        return dataset_scaled
    
    def _outputTermPCA(self, percVar, feat, nbFeat):
        """
        Prints the retained features, the number of retained features, and the maximum percentage of variance.

        Args:
        percVar (float): The maximum percentage of variance.
        feat (list): The list of retained features.
        nbFeat (int): The number of retained features.
        """
        cprint("---- Features retenues ----", "green", attrs=["bold"])
        print(feat)

        cprint("---- Nombre de features retenues ----", "green", attrs=["bold"])
        print(nbFeat)

        cprint("---- Pourcentage de variance maximum ----", "green", attrs=["bold"])
        print(f"{percVar*100} %")
        
    def pcaInverseAnalysis(self):
        """
        Performs inverse PCA analysis on the pruned dataset using the PCA model
        trained on the scaled dataset. /!\ It is LESS accurate than the OG model.

        Returns:
        --------
        - pandas.DataFrame: The inverse transformed dataset with original feature columns.
        """
        dataset_scaled = self._scaler()

        self.pca_ = PCA(n_components=self.lenFeat)
        self.pca_.fit(dataset_scaled)
        
        invDataset = self.pca_.inverse_transform(self.pruned_dataset)
        
        return pd.DataFrame(abs(invDataset), columns=self.dataset.columns)

    def pcaAnalysis(self, variance = 0.95):
        """
        Perform principal component analysis (PCA) on a given dataset and generate several outputs.

        Parameters:
        -----------
        dataset : pandas.DataFrame
            The dataset to perform PCA on.
        variance : float
            The minimum percentage of variance to be explained by the new features.

        Returns:
        --------
        - 1 dataset: the original dataset with the number of features reduced to the minimum number of features that explain the given variance.

        Outputs:
        --------
        - Prints the percentage of variance explained by each feature.
        - Saves new datasets with reduced dimensions to CSV files.
        - Saves a graph of the cumulative variance explained by each feature to a PNG file.
        - (Optional) Displays a histogram of the new components as a linear combination of the original features.

        """
        # Etude de la corrélation
        self._correlation()

        # Scale data before applying PCA => Standardisation (Normalisation des datas) avant application du PCA
        scaling=StandardScaler()
        scaling.fit(self.dataset)
        scaled_data = scaling.transform(self.dataset)
        dataset_scaled = pd.DataFrame(scaled_data, columns=scaling.get_feature_names_out())

        # Principal component analysis : Projection linéaire
        self.pca_ = PCA()
        self.pca_.fit(dataset_scaled)
        self.raw_pca_dataset = self.pca_.transform(dataset_scaled)
        
        # Cumulative variance explained by each feature
        cum_var_exp = np.cumsum(self.pca_.explained_variance_ratio_)
        cum_var_cap = [i for i in cum_var_exp if i < variance]

        # Output term for the PCA decision
        maxvar = cum_var_cap[-1]
        self.lenFeat =  len(cum_var_cap)-1
        self._outputTermPCA(maxvar, self.pca_.explained_variance_ratio_[:self.lenFeat], self.lenFeat)

        # Pruning the negligeable features
        self.pruned_dataset = self.raw_pca_dataset[:,: self.lenFeat]

        # Creation of a new dataset with the X features
        dataset_pca = pd.DataFrame(self.raw_pca_dataset[:,: self.lenFeat], columns=[f"PC{i}" for i in range(self.lenFeat)])
        dataset_pca.to_csv(f"datasets/features_pca{self.lenFeat}.csv", sep=";", index=False)

        # Vouvelles composantes comme combinaison linéaire des features
        # Choisies de manière à les avoir de manière orthogonale
        pca_df = pd.DataFrame(data = abs(self.pca_.components_[:self.lenFeat]), columns=self.pca_.feature_names_in_, index= [f"Principal Component {i}" for i in range(self.lenFeat)])
        pca_df.to_csv(f"datasets/pcaFtoF{self.lenFeat}_components.csv", sep=";", index=True)

        fig, ax = plt.subplots()
        ax.vlines(self.lenFeat, 0, cum_var_exp[self.lenFeat], linestyle='--', color="red")
        ax.hlines(cum_var_exp[self.lenFeat], 0, self.lenFeat, linestyle='--', color="red")
        ax.semilogx([self.lenFeat], cum_var_exp[self.lenFeat], linestyle='--', marker="o", color="red", label=f"{self.lenFeat} features; variance : {cum_var_exp[self.lenFeat]*100:.2f}%")
        ax.semilogx(cum_var_exp, linestyle='--', marker="o")
        ax.set_title("Cumulative variance explained by each feature")
        ax.set_xlabel('number of components')
        ax.set_ylabel('cumulative explained variance')
        ax.grid(True, which="both", ls="-")
        ax.legend()
        if os.getenv("DEMO", "0") == "1":
            plt.show()
        else:
            plt.savefig(f"graphs/pca{self.lenFeat}.png", dpi=150)
        plt.close()

        return self.raw_pca_dataset[:,:self.lenFeat]

    def tsneAnalysis(self, variance = 0.95):
        """
        Perform T-distributed Stochastic Neighbor Embedding (TSNE) analysis on the dataset.

        Parameters:
        -----------
            variance (float, optional): The desired cumulative variance explained by each feature. Defaults to 0.95.

        Returns:
        --------
            numpy.ndarray: The transformed dataset after TSNE analysis.
        """
        # T-distributed Stochastic Neighbor Embedding (TSNE)
        self.tsne_ = TSNE(n_iter=10000)
        self.raw_tsne_dataset = self.tsne_.fit_transform(self.dataset)
        
        print(self.raw_tsne_dataset)
        cprint("---- Learning rate for the TSNE ----", "green", attrs=["bold"])
        print(self.tsne_.learning_rate_)
        cprint("---- Number of iterations for the TSNE ----", "green", attrs=["bold"])
        print(self.tsne_.n_iter_)
        cprint("---- Kullback-Leibler divergence for the TSNE ----", "green", attrs=["bold"]) # Doit tendre vers 0
        print(self.tsne_.kl_divergence_)

        # Pruning the negligeable features => No pruning in the TSNE analysis
        self.pruned_dataset = self.raw_tsne_dataset
        return self.raw_tsne_dataset

        